import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count : 0
    }
  }

  plus() {
    this.setState({
      count : this.state.count + 1
    })
  }

  minus() {
    this.setState({
      count : this.state.count - 1
    })
  }

  multiplication() {
    this.setState({
      count : this.state.count * 2
    })
  }

  division() {
    this.setState({
      count : this.state.count / 2
    })
  }

  reset() {
    this.setState({
      count : 0
    })
  }

  render() {
    return (
      <span>
        <div>
          count : {this.state.count}
        </div>

        <div>
          <button onClick={this.plus.bind(this)}>Plus</button>
          <button onClick={this.minus.bind(this)}>Minus</button>
          <button onClick={this.multiplication.bind(this)}>Multi</button>
          <button onClick={this.division.bind(this)}>Div</button>
          <button onClick={this.reset.bind(this)}>Reset</button>
        </div>
      </span>
    )
  }
}

export default App;
